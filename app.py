from flask import Flask, request, json
from datetime import datetime
import requests
import re
import difflib

app = Flask(__name__)
post_params = {"query":"{business(id:\"QnVzaW5lc3NOb2RlOjE0Mjc2Y2FhLTA4NmEtNGVmNi04NzMxLTNmYWUzMjE3ZjVlZQ==\"){id name businessTransactions{edges{node{id payables{edges{node{id amount referenceId dateOccurred}}}}}}}}","variables":"null"}


@app.route("/linking_recommendations", methods=['POST'])
def get_linking_recommendations():
    req_data = json.loads(request.data)
    payment = build_payment(req_data)
    payables = load_business_payables()
    tx_ids = reconcile(payment, payables)
    return json.dumps({'transaction_ids': tx_ids})


def build_payment(req_data):
    amount = req_data.get('amount')
    payment_reference = req_data.get('payment_reference')
    payment_date = req_data.get('payment_date')
    pay_date = datetime.strptime(payment_date, '%Y-%m-%d')
    payment = {'amount': amount, 'reference': payment_reference, 'pay_date': pay_date}
    return payment


def reconcile(payment, payables):
    filtered_payables = run_filters(payment, payables)
    return build_txid(filtered_payables)


def run_filters(payment, payables):
    filtered_payables = filter_by_reference(payment, payables)
    if len(filtered_payables) == 1:
        return filtered_payables

    filtered_payables = filter_by_date(payment, filtered_payables)
    if len(filtered_payables) == 1:
        return filtered_payables

    filtered_payables = filter_by_amount(payment, filtered_payables)
    if len(filtered_payables) == 1:
        return filtered_payables

    filtered_payables = filter_by_amount_and_date(payment, filtered_payables)
    if len(filtered_payables) == 1:
        return filtered_payables

    return filtered_payables


def filter_by_reference(payment, payables):
    # filter by reference id
    reference_ids = re.split(' ', payment['reference'])
    reference_filtered = list(
        filter(lambda payable:
               payable['referenceId'] in reference_ids or similar_in(payable['referenceId'], reference_ids),
               payables)
    )
    if len(reference_filtered) == 0:
        return payables
    else:
        return reference_filtered


def filter_by_date(payment, filtered_payables):
    # filter by date
    date_filtered = list(
        filter(lambda payable: payable['dateOccurred'] < payment['pay_date'], filtered_payables)
    )
    if len(date_filtered) == 0:
        return filtered_payables
    else:
        return date_filtered


def filter_by_amount(payment, filtered_payables):
    # filter by amount
    amount_filtered = list(
        filter(lambda payable: payable['amount'] == payment['amount'], filtered_payables)
    )
    if len(amount_filtered) == 0:
        return filtered_payables
    else:
        return amount_filtered


def filter_by_amount_and_date(payment, filtered_payables):
    close_amount_date_filtered = list(
        filter(lambda payable: (payable['dateOccurred'] - payment['pay_date']).days < 30 and
                               abs(payable['amount'] - payment['amount']) < 10, filtered_payables)
    )
    if len(close_amount_date_filtered) == 0:
        return filtered_payables
    else:
        return close_amount_date_filtered


def similar_in(seq1, strings):
    for s in strings:
        if similar(seq1, s):
            return True
    return False


def similar(seq1, seq2):
    ratio = difflib.SequenceMatcher(a=seq1.lower(), b=seq2.lower()).ratio()
    return ratio > 0.86


def build_txid(payables):
    return [payable['txid'] for payable in payables]


def load_business_payables():
    response = requests.get('https://web-backend-dev.zeitgold.com/graphql', post_params)
    json_data = json.loads(response.text)
    business_transactions = json_data['data']['business']['businessTransactions']['edges']
    payables = build_payables(business_transactions)
    return payables


def build_payables(business_transactions):
    payables = []
    for btx in business_transactions:
        payable_tx_list = btx['node']['payables']['edges']
        for ptx in payable_tx_list:
            payable = ptx['node'].copy()
            payable['dateOccurred'] = datetime.strptime(ptx['node']['dateOccurred'], '%Y-%m-%d')
            payable['txid'] = btx['node']['id']
            payables.append(payable)
    return payables


if __name__ == '__main__':
    app.run()
